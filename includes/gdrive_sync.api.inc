<?php
/**
 * @file
 * File with public module functions.
 */

/**
 * Synchronize files with Google Drive.
 *
 * @param Google_DriveService $service
 *   Registered Google_DriveService object. Get one from
 *   gdrive_sync_get_service($client).
 *
 * @return numeric
 *   Returns number of synchronized files.
 */
function gdrive_sync_synchronize_files(Google_DriveService $service) {
  gdrive_sync_db_erase_all_files();
  // Take maximum number of elements.
  $list = $service->files->list(array('maxResults' => 1000));
  $updated_files = 0;
  foreach ($list['items'] as $item) {
    if (gdrive_sync_db_insert_file($item)) {
      $updated_files++;
    }
  }

  return $updated_files;
}

/**
 * Get from local database and associate access token with client.
 *
 * @param Google_Client $client
 *   Google_Client object of current client.
 * @param string $code
 *   (optional) Access code returned from Google API server ($_REQUEST['code']).
 *
 * @return Google_Client
 *   Updated Google_Client object.
 */
function gdrive_sync_associate_access_token(Google_Client $client, $code = NULL) {
  $access_token = variable_get('gdrive_sync_access_token', FALSE);
  // If access token is not stored - we need access code.
  if (!$access_token && !empty($code)) {
    $access_token = $client->authenticate($code);
    variable_set('gdrive_sync_access_token', $access_token);
  }
  $client->setAccessToken($access_token);

  if ($client->isAccessTokenExpired()) {
    $client->refreshToken($client->getRefreshToken());
    variable_set('gdrive_sync_access_token', $client->getAccessToken());
  }

  return $client;
}

/**
 * Reset locally stored credentials for Google Drive API.
 */
function gdrive_sync_reset_credentials() {
  variable_del('gdrive_sync_client_code_requested');
  variable_del('gdrive_sync_access_token');
}

/**
 * Register user from given attributes.
 *
 * @param string $client_id
 *   Client ID taken from Google API console.
 * @param string $secret
 *   Client secret taken from Google API console.
 * @param string $redirect_url
 *   (optional) URL where user will be redirected to after requesting access
 *   code.
 *
 * @return Google_Client
 *   Returns new Google_Client object.
 */
function gdrive_sync_get_client($client_id, $secret, $redirect_url = NULL) {
  $client = new Google_Client();
  $client->setClientId($client_id);
  $client->setClientSecret($secret);
  $client->setScopes(array('https://www.googleapis.com/auth/drive'));
  if (empty($redirect_url)) {
    $client->setRedirectUri(url(current_path(), array('absolute' => TRUE)));
  }
  else {
    $client->setRedirectUri($redirect_url);
  }

  return $client;
}

/**
 * Register Google Drive service for given client.
 *
 * @param Google_Client $client
 *   Google_Client object of current client.
 *
 * @return Google_DriveService
 *   Returns new Google_DriveService associated to given client.
 */
function gdrive_sync_get_service(Google_Client $client) {
  $service = new Google_DriveService($client);

  return $service;
}
