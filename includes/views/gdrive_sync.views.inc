<?php
/**
 * @file
 * File with Views representation.
 */

/**
 * Implements hook_views_data().
 */
function gdrive_sync_views_data() {
  $data['gdrive_sync_files']['table']['group'] = t('Google');

  $data['gdrive_sync_files']['table']['base'] = array(
    'field' => 'fid',
    'title' => t('Gdrive file'),
    'help' => t("gdrive files"),
    'weight' => -10,
  );

  $data['gdrive_sync_files']['fid'] = array(
    'title' => t('Google file ID'),
    'help' => t('Google drive file'),
    'group' => t('Google'),
    'field' => array('handler' => 'views_handler_field'),
  );

  $data['gdrive_sync_files']['url'] = array(
    'title' => t('Google Drive URL'),
    'group' => t('Google'),
    'field' => array('handler' => 'views_handler_field'),
  );

  $data['gdrive_sync_files']['title'] = array(
    'title' => t('Google Drive title'),
    'group' => t('Google'),
    'field' => array('handler' => 'views_handler_field'),
  );
  return $data;
}
