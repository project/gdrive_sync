<?php
/**
 * @file
 * Admin settings file.
 */

/**
 * Render form for admin settings.
 */
function gdrive_sync_admin_settings($form, &$form_state) {
  $form['gdrive_sync_clientid'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('gdrive_sync_clientid', ''),
    '#title' => t('Client Id'),
  );
  $form['gdrive_sync_secret'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('gdrive_sync_secret', ''),
    '#title' => t('Secret Key'),
  );
  $form['gdrive_sync_redirect_url'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('gdrive_sync_redirect_url', ''),
    '#title' => t('Redirect URL'),
    '#description' => t('You should set this field only for localhost workaround.'),
  );

  $form['inline_container'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),

    'gdrive_sync_synchronize' => array(
      '#type' => 'submit',
      '#value' => t('Synchronize'),
      '#submit' => array('_gdrive_sync_synchronize_submit', 'system_settings_form_submit'),
    ),
    'gdrive_sync_reset_credentials' => array(
      '#type' => 'submit',
      '#value' => t('Reset credentials'),
      '#submit' => array('gdrive_sync_reset_credentials'),
    ),
  );

  $access_token_status = '';
  if (variable_get('gdrive_sync_access_token', FALSE)) {
    $access_token_status = t('Access token is stored locally.');
  }
  elseif (!empty($_REQUEST['code'])) {
    $access_token_status = t('Press "Synchronize" button again to get your access token.');
  }
  $form['gdrive_sync_access_token_status'] = array(
    '#type' => 'item',
    '#title' => $access_token_status,
  );

  return system_settings_form($form);
}

/**
 * Submit callback for 'Synchronize' button.
 */
function _gdrive_sync_synchronize_submit($form, $form_state) {
  $client_id = $form['gdrive_sync_clientid']['#value'];
  $secret = $form['gdrive_sync_secret']['#value'];
  $redirect_url = $form['gdrive_sync_redirect_url']['#value'];
  $code = NULL;
  if (!empty($_REQUEST['code'])) {
    $code = $_REQUEST['code'];
  }

  $client = gdrive_sync_get_client($client_id, $secret, $redirect_url);
  if (variable_get('gdrive_sync_client_code_requested', FALSE)) {
    $service = gdrive_sync_get_service($client);
    gdrive_sync_associate_access_token($client, $code);
    $updated_files = gdrive_sync_synchronize_files($service);
    $message = t('@count files were successfully updated.', array('@count' => $updated_files));
    drupal_set_message($message, 'status');
  }
  else {
    variable_set('gdrive_sync_client_code_requested', TRUE);
    drupal_goto($client->createAuthUrl());
  }
}
