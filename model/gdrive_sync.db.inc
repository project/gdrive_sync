<?php
/**
 * @file
 * File with database queries.
 */

/**
 * Insert file into database.
 */
function gdrive_sync_db_insert_file($item) {
  $is_folder = $item['mimeType'] == 'application/vnd.google-apps.folder';
  $is_trashed = $item['labels']['trashed'];
  if (!$is_folder && !$is_trashed) {
    $dbitem = array(
      'gfid' => $item['id'],
      'title' => $item['title'],
      'url' => $item['alternateLink'],
    );
    db_insert('gdrive_sync_files')
      ->fields($dbitem)
      ->execute();
    return TRUE;
  }
  return FALSE;
}

/**
 * List all Google Drive files from database.
 */
function gdrive_sync_db_files_list() {
  return db_select('gdrive_sync_files', 'g')
    ->fields('g', array('gfid', 'title'))
    ->execute()
    ->fetchAllKeyed();
}

/**
 * Return file info from database.
 */
function gdrive_sync_db_get_file_info($gfid) {
  return db_select('gdrive_sync_files', 'g')
    ->fields('g', array('gfid', 'title', 'url'))
    ->condition('gfid', $gfid)
    ->execute()
    ->fetchObject();
}

/**
 * Erase 'gdrive_sync_files' table content.
 */
function gdrive_sync_db_erase_all_files() {
  return db_delete('gdrive_sync_files')
    ->execute();
}
